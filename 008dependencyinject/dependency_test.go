package _08dependencyinject

import (
	"bytes"
	"testing"
)

func TestGreet(t *testing.T) {

	buffer := bytes.Buffer{}
	Greet(&buffer, "Himura")

	got := buffer.String()
	want := "Hello, Himura"

	if got != want {
		t.Errorf("got %q want %q", got, want)
	}
}
