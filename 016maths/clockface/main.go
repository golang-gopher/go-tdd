package main

import (
	"os"
	"tdd/016maths/svg"
	"time"
)

func main() {
	t := time.Now()
	svg.Write(os.Stdout, t)
}
