package _20genericslices

func Reduce[A any](collection []A, accumulator func(A, A) A, initialValue A) A {
	var result = initialValue

	for _, x := range collection {
		result = accumulator(result, x)
	}
	return result
}

func Sum(numbers []int) int {

	add := func(acc, x int) int { return acc + x }

	return Reduce(numbers, add, 0)
}

func SumAll(numbersToSum ...[]int) []int {
	var sums []int

	for _, numbers := range numbersToSum {
		sums = append(sums, Sum(numbers))
	}

	return sums

}

func SumAllTails(numberToSum ...[]int) []int {

	sumTail := func(acc, x []int) []int {
		if len(x) == 0 {
			return append(acc, 0)
		} else {
			tail := x[1:]
			return append(acc, Sum(tail))
		}
	}

	return Reduce(numberToSum, sumTail, []int{})
}
