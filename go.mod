module tdd

go 1.19

require (
	github.com/approvals/go-approval-tests v0.0.0-20220530063708-32d5677069bd // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/gomarkdown/markdown v0.0.0-20230312001534-ae1a42e38ef1 // indirect
	github.com/kisielk/errcheck v1.6.3 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/stretchr/testify v1.8.2 // indirect
	github.com/yuin/goldmark v1.4.13 // indirect
	golang.org/x/mod v0.8.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/tools v0.6.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
