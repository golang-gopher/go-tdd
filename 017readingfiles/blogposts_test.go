package _17readingfiles

import (
	"errors"
	"io/fs"
	"reflect"
	"testing"
	"testing/fstest"
)

func TestBlogPosts(t *testing.T) {

	t.Run("happy path", func(t *testing.T) {
		// Given
		fileSystem := fstest.MapFS{
			"hello-world.md": {Data: []byte(`Title: Hello, TDD world!
Description: lol
Tags: tdd, go
---
Hello
World
`)},
			//"hello-twitch.md": {Data: []byte("Title: Hello twitchy world")},
		}

		// When
		posts, err := PostsFromFs(fileSystem)

		// Then
		if err != nil {
			t.Fatal(err)
		}

		if len(posts) != len(fileSystem) {
			t.Errorf("expected %d posts, got %d posts", len(posts), len(fileSystem))
		}

		assertPost(t, posts[0], Post{
			Title:       "Hello, TDD world!",
			Description: "lol",
			Tags:        []string{"tdd", "go"},
			Body: `Hello
World`,
		})
	})

	t.Run("failing fs", func(t *testing.T) {

		_, err := PostsFromFs(FailingFs{})

		if err == nil {
			t.Error("expected a error , didnt get one")
		}
	})
}

func assertPost(t *testing.T, got, want Post) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("got %#v, want %#v", got, want)
	}
}

type FailingFs struct {
}

func (f FailingFs) Open(_ string) (fs.File, error) {
	return nil, errors.New("oh no i always fail")
}
